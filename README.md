# Setup
 - sudo apt install systemd-container
 - sudo ./setup.sh

# Running
 - sudo  machinectl shell debian@phoshDev
 - export DISPLAY=:0
 - /usr/bin/phoc -C /usr/share/phosh/phoc.ini -E "bash -lc 'gnome-session --disable-acceleration-check --session=phosh --builtin'"
