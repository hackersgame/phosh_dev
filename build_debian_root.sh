#!/bin/bash
#sudo apt install debootstrap
cd "$(dirname "$0")"

if test -z "$SUDO_USER"
then
    USER=$(getent passwd $PKEXEC_UID | cut -d: -f1)
    echo "Skip interface (Running with pkexec)"
    run_setup="0"
else
    USER=$SUDO_USER
    run_setup="1"
fi
shell_root=/var/lib/container/debian_clean

#Only run setup interface if run with sudo not pkexec
if [ "$run_setup" -eq "1" ]
then
    if [ -d "$shell_root" ]; then
        echo "$shell_root found!"
        read -p "Would you like to rebuild $shell_root? " -n 1 -r
        echo    # (optional) move to a new line
        if [[ ! $REPLY =~ ^[Yy]$ ]]
        then
            echo "Exiting"
            exit 1
        else
            echo "Cleaning up old root..."
            rm -r $shell_root
        fi
    fi
else
    #clean up if an old install is around (Run with pkexec)
    rm -r "$shell_root"
fi

if [ "aarch64" == $(uname -m) ]
then
    echo "Running on aarch64"
    debootstrap --include python3 --arch arm64 bullseye $shell_root http://ftp.us.debian.org/debian
    
else
    echo "Running on amd64"
    debootstrap --include python3 --arch amd64 bullseye $shell_root http://ftp.us.debian.org/debian
fi


echo "Running setup script"
for i in dev sys proc
do
  mount --rbind /$i $shell_root/$i
done
cp ./setup_chroot.sh $shell_root
chroot $shell_root /setup_chroot.sh

echo "Cleaning up"
for i in dev sys proc
do
  mount --make-rslave $shell_root/$i
  umount -R $shell_root/$i
done
