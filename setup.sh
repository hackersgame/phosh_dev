#!/bin/bash

if [ "$EUID" -ne 0 ]
then 
  echo "Please run as root"
  exit
fi

cd "$(dirname "$0")"
#alarm/123456
#URL="https://github.com/dreemurrs-embedded/Pine64-Arch/releases/download/20210713/archlinux-pinephone-phosh-20210713.img.xz"
#URL=https://arm01.puri.sm/job/Images/job/Image%20Build/lastSuccessfulBuild/artifact/librem5r4.img.xz


echo "Building debain root..."
./build_debian_root.sh


echo "Running cp to /var/lib/container/phoshDev"
mkdir -p /var/lib/container/phoshDev
cp -ar /var/lib/container/debian_clean/* /var/lib/container/phoshDev

echo "Setting up nspawn"
cp -r ./systemd/* /etc/systemd/
#mkdir -p /etc/systemd/nspawn/
systemctl daemon-reload


echo "Enable service"
systemctl enable systemd-nspawn@phoshDev.service
systemctl start systemd-nspawn@phoshDev.service
