#!/bin/bash
#run inside the new chroot
user=debian
useradd $user
#clear passwd
sed -i "s/$user:\!:/$user::/g" /etc/shadow

#Setup shell
usermod --shell /bin/bash $user

#Disable login
#mkdir /etc/systemd/system/console-getty.service.d/
#echo -e "\n[Service]\nExecStart=\nExecStart=-/sbin/agetty --noclear --autologin $user --keep-baud console 115200,38400,9600 \$TERM\n" > /etc/systemd/system/console-getty.service.d/override.conf
#systemctl enable console-getty


#setup home
echo "Setting up home"
mkdir /home/$user
chown -R $user:$user /home/$user


#Install phosh (does not work with debootstrap)
apt install -y phosh
